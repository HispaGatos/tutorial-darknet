# Install latest Weechat version
sudo apt-key adv --keyserver keys.gnupg.net --recv-keys 11E9DE8848F2B65222AA75B8D1820DB22A11534E
sudo add-apt-repository "deb https://weechat.org/ubuntu $(lsb_release -cs) main"
sudo apt-get install -y weechat
mkdir ~/.weechat
cp ~/tutorial-darknet/irc.conf ~/.weechat/irc.conf
echo "That's it. you can run now weechat and it will auto connect to 10.8.0.1 port 6668"
echo "it will auto connect to some channels like #hispagatos"
echo "it may take a while since your i2p server is only running 1-2 hours or less.."
